﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Micro
{
    /// <summary>
    /// Handles object pooling for re-usability.
    /// </summary>
    /// <typeparam name="T">Type of object</typeparam>
    public class ObjectPool<T> where T : new()
    {
        /// <summary>
        /// Collection to store in-active/collected/free objects. 
        /// </summary>
        private readonly Stack<T> m_Stack;// = new Stack<T>();

        /// <summary>
        /// Returns count of all instantiated objects.
        /// </summary>
        public int countAll { get; private set; }

        /// <summary>
        /// Returns count of all active objects.
        /// </summary>
        public int countActive { get { return countAll - countInactive; } }

        /// <summary>
        /// Returns count of all inactive objects.
        /// </summary>
        public int countInactive { get { return m_Stack.Count; } }

        /// <summary>
        /// Constructor.
        /// </summary>
        public ObjectPool()
        {
            m_Stack = new Stack<T>();
        }

        /// <summary>
        /// Constructor with initial capacity of stack.
        /// </summary>
        /// <param name="a_capacity">initial capacity</param>
        public ObjectPool(int a_capacity)
        {
            m_Stack = new Stack<T>(a_capacity);
        }

        /// <summary>
        /// Returns an object. If no object found, then create new and then return.
        /// </summary>
        /// <returns>object</returns>
        public T Get()
        {
            T element;
            if (m_Stack.Count == 0)
            {
                element = new T();
                countAll++;
            }
            else
            {
                element = m_Stack.Pop();
            }

            return element;
        }

        /// <summary>
        /// Register new object created from outside.
        /// </summary>
        /// <param name="element">Object to register</param>
        public void Register(T element)
        {
            if (m_Stack.Count == 0)
            {
                countAll++;
            }
        }

        /// <summary>
        /// Pop an object from collection.
        /// </summary>
        /// <returns>Object to return</returns>
        public T GetPop()
        {
            if (m_Stack.Count > 0)
            {
                return m_Stack.Pop();
            }

            return default(T);
        }

        /// <summary>
        /// Release object and hand over to collection.
        /// </summary>
        /// <param name="element">Object to release</param>
        public void Release(T element)
        {
            if (m_Stack.Count > 0 && ReferenceEquals(m_Stack.Peek(), element))
                Debug.LogError("Internal error. Trying to destroy object that is already released to pool.");

            m_Stack.Push(element);
        }

        /// <summary>
        /// Create a number of objects in advance to use them later.
        /// </summary>
        /// <param name="a_count">Number of objects to create</param>
        public void CreateAndKeep(int a_count)
        {
            for (int l_index = 0; l_index < a_count; l_index++)
            {
                m_Stack.Push(new T());
                countAll++;
            }
        }

        /// <summary>
        /// Return True, if object is present in collection.
        /// </summary>
        /// <param name="a_element">object to check</param>
        /// <returns>True, if object is present in collection</returns>
        internal bool Contains(T a_element)
        {
            return m_Stack.Contains(a_element);
        }

        /// <summary>
        /// Get enumerator for traversing outside.
        /// </summary>
        /// <returns></returns>
        internal System.Collections.Generic.IEnumerator<T> GetEnumerator()
        {
            return m_Stack.GetEnumerator();
        }

        /// <summary>
        /// Return string contains countAll, countActive and countInactive.
        /// </summary>
        /// <returns>String contains all count values</returns>
        internal string GetAllCountsStr()
        {
            return "All:" + countAll + ", Active:" + countActive + ", Inactive:" + countInactive;
        }
    }
}

