﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Micro.Editor
{
    public class ObjectPoolTypes : MonoBehaviour
    {
        public static ObjectPool<Rect> ObjectPool_Rect = new ObjectPool<Rect>();
        public static ObjectPool<GUIContent> ObjectPool_GUIContent = new ObjectPool<GUIContent>();
    }
}