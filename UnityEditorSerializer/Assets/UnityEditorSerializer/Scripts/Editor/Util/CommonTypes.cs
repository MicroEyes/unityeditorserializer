﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Micro
{
    public enum EAccessSpecifier
    {
        Private,
        Public,
        Internal,
        Protected,
        ReadNoWrite
    }
}