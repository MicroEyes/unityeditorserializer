﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Reflection;
using System.Linq;
using System.Text;
using System;

namespace Micro.Editor
{
    //[InitializeOnLoad]
    //[CustomEditor(typeof(SerializedMonoBehaviour), true)]
    public class Editor_SerializedMonoBehaviour : UnityEditor.Editor
    {
        static MemberInfo[] s_monoBehaviorMembers;
        static Editor_SerializedMonoBehaviour()
        {
            Debug.Log("Editor_SerializedMonoBehaviour");
            s_monoBehaviorMembers = typeof(MonoBehaviour).GetMembers(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            Debug.Log("Monobeviour member count " + s_monoBehaviorMembers.Length);
        }

        SerializedMonoBehaviour m_target;
        List<MemberInfo> m_lstMemberInfo;
        List<SerializedProperty> m_lstSerializedProperties;
        private void OnEnable()
        {
            Debug.Log("OnEnable");
            m_target = target as SerializedMonoBehaviour;

            MemberInfo[] l_arrmemberInfoTemp = m_target.GetType().GetMembers(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            Debug.Log(l_arrmemberInfoTemp.Length);

            m_lstMemberInfo = new List<MemberInfo>();
            foreach (MemberInfo l_memberInfo in l_arrmemberInfoTemp)
            {
                if (!s_monoBehaviorMembers.Any(l_item => l_item.Name.Equals(l_memberInfo.Name))
                    && l_memberInfo.MemberType != MemberTypes.Method)
                {
                    m_lstMemberInfo.Add(l_memberInfo);
                }
            }

            l_arrmemberInfoTemp = m_lstMemberInfo.ToArray();
            StringBuilder l_builder = new StringBuilder();
            foreach (var l_item in l_arrmemberInfoTemp)
            {
                l_builder.AppendLine(l_item.Name);
            }
            Debug.Log("MemberCount: " + l_arrmemberInfoTemp.Length + "\n" + l_builder.ToString());

            //m_lstSerializedProperties = new List<SerializedProperty>();
            //foreach (MemberInfo l_memberInfo in l_arrmemberInfoTemp) 
            //{
            //    if(l_memberInfo.MemberType == MemberTypes.Field)
            //    { 
            //        m_lstSerializedProperties.Add(serializedObject.FindProperty(l_memberInfo.Name)); 
            //    }
            //}

            //Print(m_lstSerializedProperties);
            //Debug.Log("Fields: " + m_lstSerializedProperties.Count);
        }

        private void OnDisable()
        {
            Debug.Log("OnDisable");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            base.OnInspectorGUI();
            //Rect l_rectTotalPosition = new Rect(0, 0, Screen.width, 30);
            //GUI.Label(l_rectTotalPosition, new GUIContent("Satbir"));
            //return;
            /*int l_memberDrawIndex = -1;
            float yMax = 0.0f;
            for (int l_indexMemberInfo = 0; l_indexMemberInfo < m_lstMemberInfo.Count; l_indexMemberInfo++)
            {
                MemberInfo l_memberInfo = m_lstMemberInfo[l_indexMemberInfo];

                bool l_isSerializeAttributeFound = false;

                //Checking if serialize attribute.
                object[] l_attributes = l_memberInfo.GetCustomAttributes(false);
                for (int l_index = 0; l_index < l_attributes.Length; l_index++)
                {
                    Type l_type = l_attributes[l_index].GetType();
                    if (l_type.Equals(typeof(UnityEngine.SerializeField)) || l_type.Equals(typeof(ShowInInspectorAttribute)))
                    {
                        l_isSerializeAttributeFound = true;
                        break;
                    }
                }

                if (!l_isSerializeAttributeFound)
                {
                    //Debug.LogWarning("Not Serializing " + l_memberInfo.Name);
                    continue;
                }

                ++l_memberDrawIndex;

                SerializedProperty l_serializeProperty = serializedObject.FindProperty(l_memberInfo.Name);
                Type l_dataType = GetMemberDataType(l_memberInfo);

                if (l_dataType == typeof(int))
                {
                    Drawer_Number.Draw(l_memberInfo, l_serializeProperty, l_memberDrawIndex, ref yMax);
                }
                else if (l_dataType == typeof(float))
                {

                }
            }
            */
            serializedObject.ApplyModifiedProperties();

        }

        private Type GetMemberDataType(MemberInfo a_memberInfo)
        {
            if (a_memberInfo is PropertyInfo)
            {
                return ((PropertyInfo)a_memberInfo).PropertyType;
            }
            else if (a_memberInfo is FieldInfo)
            {
                return ((FieldInfo)a_memberInfo).FieldType;

            }
            else
                throw new Exception("Invalid type '" + a_memberInfo + "'");
        }

        public static EAccessSpecifier GetAccessSpecifier(MemberInfo a_memberInfo)
        {
            if (a_memberInfo is PropertyInfo)
            {
                //A member info could be representing a property, and properties do not have an accessibility. 
                //(The getter and setter have an accessibility, but since they can be different, the property does not have an accessibility.

                PropertyInfo l_propertyInfo = (PropertyInfo)a_memberInfo;
                if (l_propertyInfo.CanRead && l_propertyInfo.CanWrite)
                    return EAccessSpecifier.Public;
                else if (l_propertyInfo.CanRead && !l_propertyInfo.CanWrite)
                    return EAccessSpecifier.ReadNoWrite;
                else
                    return EAccessSpecifier.Private;
            }
            else if (a_memberInfo is FieldInfo)
            {
                FieldInfo l_fieldInfo = (FieldInfo)a_memberInfo;
                if (l_fieldInfo.IsPublic)
                    return EAccessSpecifier.Public;
                else if (l_fieldInfo.IsAssembly)
                    return EAccessSpecifier.Internal;
                else
                    return EAccessSpecifier.Private;
            }
            else
                throw new Exception("MemberInfo '" + a_memberInfo.Name + "' is not a field or property");
        }

        void Print(List<SerializedProperty> a_serializedProperties)
        {
            StringBuilder l_builder = new StringBuilder();

            foreach (SerializedProperty l_item in a_serializedProperties)
            {
                l_builder.AppendLine(l_item == null ? "null" : l_item.name);
            }

            Debug.Log(l_builder.ToString());
        }
    }
}