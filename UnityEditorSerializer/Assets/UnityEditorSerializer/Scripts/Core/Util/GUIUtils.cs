﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Micro.Editor
{
    public static class GUIUtils 
    {
        public static string RenameVariableName(string a_varToRename)
        {
            string a_newVariableName = a_varToRename;
            if(a_varToRename.IndexOf("m_") == 0)
            {
                a_newVariableName = a_varToRename.Remove(0, 3);
                a_newVariableName = char.ToUpper(a_varToRename[2]) + a_newVariableName;
            }

            return a_newVariableName;
        }

        public static Vector2 GetSize(GUIContent a_guiContent, string a_strStyle)
        {
            return GUI.skin.GetStyle(a_strStyle).CalcSize(a_guiContent);
        }
    }
}