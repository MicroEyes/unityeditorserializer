﻿using Micro.Editor;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


namespace Micro
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public sealed class WidthAttribute : MultiPropertyAttribute
    {
        const float DEFAULT_SPACE_BETWEEN_FEILD_VALUE = 5.0f;
        private float? m_widthLabel;
        private float m_width;
        public float Width
        {
            get { return m_width; }
        }

        public WidthAttribute(float a_width)
        {
            m_width = a_width;
        }

        public WidthAttribute(float a_widthLabel, float a_width)
        {
            m_widthLabel = a_widthLabel;
            m_width = a_width;
        }

        public override void OnPreGUI(ref Rect position, SerializedProperty property)
        {
            position.width = m_width;

            base.OnPreGUI(ref position, property);
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (!m_widthLabel.HasValue)
            {
                EditorGUIUtility.labelWidth = GUIUtils.GetSize(label, "label").x + DEFAULT_SPACE_BETWEEN_FEILD_VALUE;
            }

            base.OnGUI(position, property, label);
        }
    }
}