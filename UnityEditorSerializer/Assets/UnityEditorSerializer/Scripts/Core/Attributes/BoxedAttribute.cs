﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[AttributeUsage(AttributeTargets.Field)]
public class BoxedAttribute : MultiPropertyAttribute
{
    const int DEFAULT_PADDING = 2;
    private string GUIstyle = "box";
    private int m_Left = DEFAULT_PADDING;
    private int m_Right= DEFAULT_PADDING;
    private int m_Top= DEFAULT_PADDING;
    private int m_Bottom= DEFAULT_PADDING;
    public BoxedAttribute() { }
    public BoxedAttribute(int left, int right, int top, int bottom)
    {
        m_Left = left;
        m_Right = right;
        m_Top = top;
        m_Bottom = bottom;
    }

#if UNITY_EDITOR
    public override void OnPreGUI(ref Rect a_position, SerializedProperty a_property)
    {
        if (string.IsNullOrEmpty(GUIstyle))
            GUI.Box(EditorGUI.IndentedRect(a_position), GUIContent.none);
        else
            GUI.Box(EditorGUI.IndentedRect(a_position), GUIContent.none, GUIstyle);
        var offset = new RectOffset(m_Left, m_Right, m_Top, m_Bottom);
        a_position = offset.Remove(a_position);
    }

    public override float? GetPropertyHeight(SerializedProperty property, GUIContent label, float a_height)
    {
        return a_height + m_Top + m_Bottom;
    }
#endif
}
