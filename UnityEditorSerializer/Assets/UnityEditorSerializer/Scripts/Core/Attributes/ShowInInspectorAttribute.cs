﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Micro
{
    public sealed class ShowInInspectorAttribute : Attribute
    {
        public ShowInInspectorAttribute() { }
    }
}