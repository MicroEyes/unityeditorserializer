﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

[AttributeUsage(AttributeTargets.Field)]
public abstract class MultiPropertyAttribute : PropertyAttribute
{
#if UNITY_EDITOR
    public IOrderedEnumerable<object> stored = null;

    public virtual GUIContent BuildLabel(GUIContent label)
    {
        return label;
    }

    public virtual void OnPreGUI(ref Rect position, SerializedProperty property) { }
    
    public virtual void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.PropertyField(position, property, label);
    }

    public virtual void OnPostGUI(Rect position, SerializedProperty property) { }

    public virtual bool IsVisible(SerializedProperty property){return true;}
    public virtual float? GetPropertyHeight(SerializedProperty property, GUIContent label, float a_height)
    {
        return null;
    }
#endif
}