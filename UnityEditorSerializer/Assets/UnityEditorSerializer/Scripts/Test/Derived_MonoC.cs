﻿using Micro;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMyInterface
{
    string myProp
    {
        get; set;
    }
}

public class Derived_MonoC : SerializedMonoBehaviour
{
    [Boxed(5,5,5,5)]
    [SerializeField]
    int m_int = 10;

    [Boxed]
    [SerializeField]
    float m_float;


    [Width(70)]
    [SerializeField]
    int m_int2;
    public int Int2
    {
        get { return m_int2; }
        set { m_int2 = value; }
    }

    [ShowInInspector]
    IMyInterface m_iMyInterface;

    [SerializeField]
    string m_string;

    [SerializeField]
    Vector3 m_vector;
}